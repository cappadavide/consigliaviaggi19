package com.example.consigliaviaggi;

public interface PresenterToFotoDAO {

    void obtainPhotosByStructureId(int id);
}
