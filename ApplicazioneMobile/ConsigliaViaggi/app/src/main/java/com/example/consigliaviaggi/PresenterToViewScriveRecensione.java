package com.example.consigliaviaggi;

import android.content.Context;

public interface PresenterToViewScriveRecensione {

    Context getContext();
    void reviewDone();
}
