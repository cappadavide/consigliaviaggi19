package com.example.consigliaviaggi;

import android.content.Context;

public interface PresenterToViewImpostazioni {

    Context getContext();
    void notMakeLogout();
}
