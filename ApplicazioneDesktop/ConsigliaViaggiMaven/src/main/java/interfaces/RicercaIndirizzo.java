package interfaces;

import java.io.IOException;
import javafx.event.ActionEvent;

public interface RicercaIndirizzo {
    public void rispostaRicevuta(String result);
}
