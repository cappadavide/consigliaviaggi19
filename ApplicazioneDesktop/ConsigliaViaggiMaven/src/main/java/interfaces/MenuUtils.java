package interfaces;

import java.io.IOException;
import javafx.event.ActionEvent;

public interface MenuUtils {
    public void logout(ActionEvent event) throws IOException;
    public void visualizzaInformazioni(ActionEvent event);
}
